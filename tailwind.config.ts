import type { Config } from 'tailwindcss'
import {nextui} from "@nextui-org/react";
const config: Config = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
    "./node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}"
  ],
  theme: {
    extend: {
      colors: {
        'themePalettes': {
          'darkNavy': 'rgb(0, 28, 48)',
          'darkTeal': 'rgb(23, 107, 135)',
          'mint': 'rgb(100, 204, 197)',
          'cyan': 'rgb(218, 255, 251)'
        }
      },
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-conic':
          'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
        'themeLightPalette-gradient-lineal-135deg': `linear-gradient(135deg, rgba(255,255,255,1) 0%, rgba(218,255,245,1) 50%, rgba(218,255,251,1) 100%)`,
        'themeDarkPalette-gradient-lineal-135deg': `linear-gradient(135deg, rgba(0,0,0,0.125) 0%, hsla(239, 100%, 9%,0.25) 50%, hsla(205, 100%, 18%,0.5) 100%), linear-gradient(225deg, rgba(0,0,0,0.125) 0%, hsla(239, 100%, 9%,0.25) 50%, hsla(205, 100%, 18%,0.5) 100%)`,
        'themeDarkPalette-gradient-lineal': `linear-gradient(180deg, rgba(0,28,48,1) 0%, rgba(0,0,0,1) 100%)`,
        'themeLightPalette-gradient-lineal': `linear-gradient(180deg, rgba(218,255,251,1) 0%, rgba(255,255,255,1) 100%)`,
      },
      minHeight: {
        '93dvh': '93dvh'
      }
      },
  },
  darkMode: "class",
  plugins: [nextui({
    addCommonColors: false,
    themes: {
      light: {
        colors: {
          foreground: {
            DEFAULT: '#0004CE'
          },
          primary: {
            100: '#00CECA',
            200: '#009FCE',
            300: '#006BCE',
            400: '#0038CE',
            500: '#0004CE',
            600: '#3000CE',
            DEFAULT: '#0038CE'
          }
        }
      },
      dark: {
        colors: {
          background: {
            DEFAULT: '#010F18'
          },
          foreground: {
            DEFAULT: '#8732FF'
          },
          primary: {
            100: '#32AAFF',
            200: '#3277FF',
            300: '#3243FF',
            400: '#5432FF',
            500: '#8732FF',
            600: '#BA32FF',
            DEFAULT: '#5432FF',
          },
        }
      }
    }
  })]
}
export default config
