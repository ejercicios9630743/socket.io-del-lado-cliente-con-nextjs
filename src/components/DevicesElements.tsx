import React from 'react'
import { TPropRenderDeviceElement, TPropsDevicesElements } from '@/interfaces/components.interfaces/devicesElements';


export const DevicesElements: React.FC<TPropsDevicesElements> = ({elements, ComponentDeviceElement: Component, actualCategory}) => {
  const [itemRenderDevice] = elements
  return (
    <>
    {
      Object.entries(itemRenderDevice).map((item: TPropRenderDeviceElement, index) => {
        return (
          <Component key={`${index}-${item[0]}`} elementDevice={item} actualCategory={actualCategory}/>
        )
      })
    }
    </>
    
  )
}
