import { TPropsCategoryDevicesElementsWithComponent } from '@/interfaces/components.interfaces/categoryDevicesElements'
import React from 'react'
import { DeviceElement } from './DeviceElement'

export const CategoryDevicesElements: React.FC<TPropsCategoryDevicesElementsWithComponent> = ({elements, ComponentDevicesElements: Component}) => {
  const [categoryRender, itemsRenderDevices] = elements
  return (
    <section className='flex flex-col gap-2 mx-4 my-2 border-spacing-1 border-solid border-themePalettes-cyan'>
      <h1 className='font-bold capitalize text-3xl text-primary text-center'>{categoryRender}</h1>
      <div className='flex flex-row gap-4 justify-center'>
        {
        <Component elements={[itemsRenderDevices]} ComponentDeviceElement={DeviceElement} actualCategory={categoryRender}/>
        }
      </div>
      
    </section>
  )
}
