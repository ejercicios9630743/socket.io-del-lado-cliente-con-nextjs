import React, { useContext, useEffect, useState } from 'react'
import {  Navbar as NextUINavbar,   NavbarBrand,   NavbarContent,   NavbarItem,   NavbarMenuToggle,  NavbarMenu,  NavbarMenuItem, Link, Button, Image} from "@nextui-org/react";
import NextJsImage from 'next/image'
import { ThemeSwitcher } from './ThemeSwicher';
import { useTheme } from 'next-themes';
import { context } from '@/Context/CreateContext';
export const Navbar: React.FC = () => {
  const [isMenuOpen, setIsMenuOpen] = useState(false)
  const {user, changeUser} = useContext(context)
  const {theme} = useTheme()

  const menuItems = [
    'devices'
  ];

  const logoImgThemeLight = <Image as={NextJsImage} priority={true} width={97} height={97} src={'/img/Seo_bombilla.png'} alt='Logo' className='size-3/5 mx-auto'/>
  const logoImgThemeDark = <Image as={NextJsImage} priority={true} width={97} height={97} src={'/img/seo-light.png'} alt='Logo' className='size-3/5 mx-auto'/>
  return (
      <NextUINavbar onMenuOpenChange={setIsMenuOpen} isBordered isBlurred>
      <NavbarContent>
        <NavbarMenuToggle
          aria-label={isMenuOpen ? "Close menu" : "Open menu"}
          className="sm:hidden"
        />
        <NavbarBrand>
          <Link href='/' aria-current='page' className='size-3/5 gap-4'>
            {theme === undefined ? logoImgThemeLight : theme === 'light' ? logoImgThemeLight : logoImgThemeDark}
            <p className="font-bold text-inherit">Seo Electronics</p>
          </Link>
        </NavbarBrand>
      </NavbarContent>

      <NavbarContent className="hidden sm:flex gap-4" justify="center">
        <NavbarItem isActive>
          <Link href="/" aria-current="page">
            Devices
          </Link>
        </NavbarItem>
      </NavbarContent>
      <NavbarContent justify="end">
        <NavbarItem className="hidden lg:flex">
          <ThemeSwitcher/>
          <Button size='sm' variant='light' onPress={() => changeUser(user === 'user' ? 'admin' : 'user')}>{user === 'user' ? 'User' : 'Admin'}</Button>
        </NavbarItem>
      </NavbarContent>
      <NavbarMenu>
        {menuItems.map((item, index) => (
          <NavbarMenuItem key={`${item}-${index}`}>
            <Link
              color={
                index === 1 ? "primary" : "foreground"
              }
              className="w-full"
              href="#"
              size="lg"
            >
              {item}
            </Link>
          </NavbarMenuItem>
        ))}
        <NavbarMenuItem><ThemeSwitcher/></NavbarMenuItem>
      </NavbarMenu>
    </NextUINavbar>
  )
}
