// components/ThemeSwitcher.tsx
import {useTheme} from "next-themes";
import { Button } from "@nextui-org/react";
import { FaMoon } from "react-icons/fa";
import { FaSun } from "react-icons/fa";
import { useState, useEffect } from "react";

export const ThemeSwitcher: React.FC = () => {
  const [mounted, setMounted] = useState(false)
  const { theme, setTheme } = useTheme()

  // useEffect only runs on the client, so now we can safely show the UI
  useEffect(() => {
    setMounted(true)
  }, [])

  if (!mounted) {
    return null
  }

  return (
    <div>
      <Button size="sm" color="primary" variant="ghost" onClick={() => setTheme(theme === 'dark' ? 'light' : 'dark')}>{theme === 'dark' ? <FaMoon/> : <FaSun/>}</Button>
    </div>
  )
};