
import { TPropDeviceElement } from '@/interfaces/components.interfaces/deviceElement'
import { Button, Card, CardBody, CardFooter, CardHeader, Divider, Modal, ModalBody, ModalContent, ModalFooter, ModalHeader, Select, SelectItem, Selection, Slider, SliderValue, Switch, useDisclosure } from '@nextui-org/react'
import { useTheme } from 'next-themes'
import React, { useContext, useEffect, useState } from 'react'
import { TbAirConditioningDisabled } from "react-icons/tb";
import { TbAirConditioning } from "react-icons/tb";
import { TbWash } from "react-icons/tb";
import { TbWashOff } from "react-icons/tb";
import { CgSmartHomeRefrigerator } from "react-icons/cg";
import { PiTelevision } from "react-icons/pi";
import { AiOutlineSound } from "react-icons/ai";
import { TbPrinter } from "react-icons/tb";
import { TbPrinterOff } from "react-icons/tb";
import { TbDeviceLaptop } from "react-icons/tb";
import { TbDeviceLaptopOff } from "react-icons/tb";
import { context } from '@/Context/CreateContext';
import { useHover } from '@uidotdev/usehooks';

const line45DegSvg = 
<div className='absolute'>
  <svg xmlns="http://www.w3.org/2000/svg" version="1.0" width="100.000000pt" height="100.000000pt" viewBox="0 0 100.000000 100.000000" preserveAspectRatio="xMidYMid meet">
    <g transform="translate(0.000000,100.000000) scale(0.100000,-0.100000)" fill="currentColor" stroke="none">
    <path d="M17 973 c-26 -25 12 -67 452 -507 411 -410 461 -457 479 -447 50 27 50 28 -425 503 -439 439 -481 477 -506 451z"/>
    </g>
  </svg>
</div>

export const DeviceElement: React.FC<TPropDeviceElement> = ({elementDevice, actualCategory}) => {
  const { theme } = useTheme()
  const [keyDevice, itemDevice] = elementDevice
  const {user, newStateForDevice, handleSendMessage, changeContextNewStateDevice} = useContext(context)
  const [isSelected, setIsSelected] = useState(itemDevice.state === 'on' ? true : false);
  const {isOpen, onOpen, onOpenChange} = useDisclosure();
  const [valueSelectTemp, setValueSelectTemp] = useState<SliderValue>(16);
  const [onChangeStateDevice, setChangeStateDevice] = useState(false)
  const [ref, hoverable] = useHover()
  const isAdmin = user === 'user' ? true : false

  const handleSliderChangeInAirConditioningTemperature = (value: SliderValue) => {
    console.log(value)
    setValueSelectTemp(value);
    const airConditioningTemperature = value.toString()
    changeContextNewStateDevice(user, actualCategory, keyDevice, airConditioningTemperature !== '31' ? airConditioningTemperature : 'off');
    setChangeStateDevice(true);
  };
  const handleSwitchChangeInDevice = (event: React.ChangeEvent<HTMLInputElement>) => {
    const switchState = event.target.checked ? 'on' : 'off'
    changeContextNewStateDevice(user, actualCategory, keyDevice, switchState)
    setChangeStateDevice(true)
  }

  useEffect(() => {
    newStateForDevice.user === 'admin' && onChangeStateDevice ? handleSendMessage('setDevicestate', newStateForDevice) : null;
    setIsSelected(itemDevice.state === 'on' ? true : false)
    setChangeStateDevice(false)
  }, [handleSendMessage, newStateForDevice, onChangeStateDevice, isSelected, itemDevice.state]);

  let bgTheme
  if(theme === 'light'){
    bgTheme = 'bg-themeLightPalette-gradient-lineal-135deg'
  }else if(theme === 'dark') {
    bgTheme = 'bg-themeDarkPalette-gradient-lineal-135deg'
  }else if(theme === undefined){
    bgTheme = 'bg-themeLightPalette-gradient-lineal-135deg'
  }
  const tempAvailable: Array<number> = []
  
  for(let i = 16; i<= 31; i++){
    tempAvailable.push(i)
  }
  
  const switchOnOff = <Switch isSelected={isSelected} onChange={handleSwitchChangeInDevice} isDisabled={isAdmin}>{isSelected ? 'On' : 'Off'}</Switch>
  const selectTemp = <Slider
  value={valueSelectTemp}
  size='sm'
  step={1}
  minValue={16}
  maxValue={31}
  showTooltip
  onChange={handleSliderChangeInAirConditioningTemperature}
  isDisabled={isAdmin}
  aria-label="Select temperature"
  marks={[
    {
      value: 16,
      label: '16'
    },
    {
      value: 31,
      label: 'Off'
    },
  ]}
/>

  const cardFooterForSetState = <CardFooter className="justify-center before:bg-white/10 border-white/20 border-1 overflow-hidden py-1 absolute before:rounded-xl rounded-large bottom-1 w-[calc(100%_-_8px)] shadow-small ml-1 z-10">
  {keyDevice === 'airConditioner' ? selectTemp : switchOnOff}
</CardFooter>

  let icon: JSX.Element = <></>
  const addSpacesToCamelCase = (input: string): string => {
    // Expresión regular para identificar las transiciones de mayúsculas a minúsculas en camelCase
    const camelCaseRegex = /([a-z])([A-Z])/g;
  
    // Reemplaza cada coincidencia con el grupo 1 seguido de un espacio y el grupo 2
    const stringWithSpaces = input.replace(camelCaseRegex, '$1 $2');
  
    // Convierte la primera letra a mayúscula para mantener consistencia
    return stringWithSpaces.charAt(0).toUpperCase() + stringWithSpaces.slice(1);
  }
  
  switch (keyDevice) {
    case 'airConditioner':
      itemDevice.state !== 'off' ? icon = <TbAirConditioning/> : icon = <TbAirConditioningDisabled/>
      break;
    case 'washingMachine':
      itemDevice.state === 'on' ? icon = <TbWash/> : icon = <TbWashOff/>
      break
    case 'refrigerator':
      itemDevice.state === 'on' ? icon = <CgSmartHomeRefrigerator/> : icon = <>{line45DegSvg}<CgSmartHomeRefrigerator/></>
      break
    case 'tv':
      itemDevice.state === 'on' ? icon = <PiTelevision/> : icon = <>{line45DegSvg}<PiTelevision/></>
      break
    case 'soundSystem':
      itemDevice.state === 'on' ? icon = <AiOutlineSound/> : icon = <>{line45DegSvg}<AiOutlineSound/></>
      break
    case 'printer':
      itemDevice.state === 'on' ? icon = <TbPrinter/> : icon = <TbPrinterOff/>
      break
    case 'laptop':
      itemDevice.state === 'on' ? icon = <TbDeviceLaptop/> : icon = <TbDeviceLaptopOff/>
      break
    default: null
      break;
  }
  return (
    <>
    <div onClick={()=> onOpen()} className='animate__animated animate__backInRight cursor-pointer'>
      <Card
      isFooterBlurred
      isHoverable
      radius="lg"
      ref={ref}
      className={`${bgTheme} font-semibold ${hoverable ? 'hvr-grow' : ''}`}
    >
      <CardHeader>
        <span className={`font-bold text-xl text-primary-200 text-center mx-auto`}>{addSpacesToCamelCase(keyDevice)}</span>
      </CardHeader>
      <CardBody className='[&_svg]:size-40 [&_svg]:m-2 [&_svg]:text-primary-300 [&_svg]:dark:text-primary-300 [&_svg]:dark:brightness-90'>
            {icon}
      </CardBody>
      {!isAdmin ? cardFooterForSetState : null}
    </Card>
    </div>
    {/* Modal */}

    <Modal backdrop={"blur"} isOpen={isOpen} onOpenChange={onOpenChange} placement="center" scrollBehavior="outside">
    <ModalContent>
         {(onClose) => (
           <>
             <ModalHeader className="flex flex-col">
              <span className='mx-auto my-2 text-primary'>{addSpacesToCamelCase(keyDevice)}</span>
              <Divider/>
              </ModalHeader>
             <ModalBody>
              <div className='grid place-items-center justify-center [&_svg]:size-40 [&_svg]:text-primary-300 [&_svg]:dark:text-primary-300 [&_svg]:dark:brightness-90'>
                <div className='grid place-content-center'>
                  {icon}
                </div>
                <div className='flex flex-col gap-2 capitalize'>
                  {
                    Object.entries(itemDevice).map(([key,item], index)=> {
                      return (
                        <div key={index} className='grid grid-cols-2 gap-4 place-items-center'>
                        <span className='font-bold text-lg text-primary-400'>{addSpacesToCamelCase(key) + ':'}</span>
                        <span className='text-base font-semibold text-primary-300'>{keyDevice === 'printer' && key === 'color' && item === true ? 'true' : keyDevice === 'printer' && key === 'color' && item === false ? 'false' : item}</span>
                        </div>
                      )
                    })
                  }
                </div>
             
            </div>
             </ModalBody>
             <ModalFooter>
               <Button color="danger" variant="light" onPress={onClose}>
                 Close
               </Button>
             </ModalFooter>
           </>
         )}
       </ModalContent>
    </Modal>
    </>
    
  )
}
