import React from 'react'
import { TPropsGridCategoryDevicesElements } from '@/interfaces/components.interfaces/grid'


export const Grid: React.FC<TPropsGridCategoryDevicesElements> = ({elements, Component, auxiliaryComponent}) => {

  return (
    <section className='flex flex-col flex-wrap justify-center w-10/12 gap-3 mx-auto my-0'>
          {Object.entries(elements).map(([key,item], index) =>{
            if(auxiliaryComponent) {
              return (
                <div key={index} className='max-w-full'>
                  <Component elements={[key,item]} ComponentDevicesElements={auxiliaryComponent}/>
                </div>
              )
            }
      })}
    </section>
  )
}

