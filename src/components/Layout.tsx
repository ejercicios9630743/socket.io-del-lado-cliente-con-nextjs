import React, { ReactNode } from 'react'
import { Header } from './Header'
type Props = {children: ReactNode}
export const Layout: React.FC<Props> = ({children}) => {
  return (
    <>
    <Header/>
    <main className='bg-foreground-50-50 min-h-93dvh'>
    {children}
    </main>
    </>
  )
}
