import { Key } from 'react';
import { Devices } from '../device.interface';

type TPropsWithinAppliancesDevices = Devices['appliances'][keyof Devices['appliances']];
type TPropsWithinComputingDevices = Devices['computing'][keyof Devices['computing']];
type TPropsWithinMultimediaDevices = Devices['multimedia'][keyof Devices['multimedia']];

export type TPropsWithinAllCategoryDevices = TPropsWithinAppliancesDevices | TPropsWithinComputingDevices | TPropsWithinMultimediaDevices

export type TPropDeviceElement = {
  elementDevice: [string, TPropsWithinAllCategoryDevices];
  actualCategory: string
};

export interface INewStateForChangeStateDevice {
  user: 'user' | 'admin';
  category: string;
  device: string
  newState: string | Key;
}
