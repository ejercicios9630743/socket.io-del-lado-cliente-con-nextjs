import { TPropsCategoryDevicesElementsWithComponent } from "./categoryDevicesElements";
import { Devices } from "../device.interface";
import { TPropsDevicesElements } from "./devicesElements";

export type TPropsGridCategoryDevicesElements = {
  elements: Partial<Devices>, 
  Component: React.FC<TPropsCategoryDevicesElementsWithComponent>, 
  auxiliaryComponent?: React.FC<TPropsDevicesElements>
}