import { Devices } from "../device.interface";
import { TPropsDevicesElements } from "./devicesElements";

export type TPropsCategoryDevicesElementsWithComponent = {elements: [string, Devices['appliances'] | Devices['computing'] | Devices['multimedia']], ComponentDevicesElements: React.FC<TPropsDevicesElements>}
