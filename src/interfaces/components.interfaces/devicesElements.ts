import { Devices } from "../device.interface";
import { TPropDeviceElement, TPropsWithinAllCategoryDevices } from "./deviceElement";

export type TPropsDevicesElements = {elements: (Devices['appliances'] | Devices['computing'] | Devices['multimedia'])[], ComponentDeviceElement: React.FC<TPropDeviceElement>, actualCategory: string}
export type TPropRenderDeviceElement = [string, TPropsWithinAllCategoryDevices]