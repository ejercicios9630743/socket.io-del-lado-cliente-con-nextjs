import { ReactNode } from "react";

export type TPropOnlyChildren = {children: ReactNode}