import { Devices } from "../device.interface"
import { INewStateForChangeStateDevice } from "../components.interfaces/deviceElement"

export interface IInitialState {
  welcome: [] | string,
  saludo: [] | string,
  randomNumber: [] | number,
  colorName: [] | string,
  type?: string
  devices: Devices,
  user: 'admin' | 'user',
  newStateForDevice: INewStateForChangeStateDevice
}

export interface ContextValue extends IInitialState {
  handleSendMessage: (event: string, message: any) => void
  changeUser: (user: 'user' | 'admin') => void
  changeContextNewStateDevice: (user: INewStateForChangeStateDevice['user'], category:INewStateForChangeStateDevice['category'], device:INewStateForChangeStateDevice['device'], newState: INewStateForChangeStateDevice['newState']) => void
}