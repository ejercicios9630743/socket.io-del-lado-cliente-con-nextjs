import React, { useEffect, useState } from 'react';
import { Layout } from '@/components/Layout';
import { AppProps } from 'next/app';
import { StateContext } from '@/Context/StateContext';
import '../styles/globals.css'
import {NextUIProvider} from '@nextui-org/react'
import '../styles/hover.css'
import 'animate.css';
import { useRouter } from 'next/router';
import {Dosis} from 'next/font/google'
import {ThemeProvider as NextThemesProvider, useTheme} from "next-themes";

const dosis = Dosis({subsets: ['latin']})
interface MyAppProps {
  Component: React.ComponentType<AppProps>;
  pageProps: AppProps;
}

const MyApp: React.FC<MyAppProps> = ({ Component, pageProps }) => {
  const router = useRouter();
  const {setTheme} = useTheme()
  setTheme('light')
  return (
    <>
      <NextUIProvider navigate={router.push}>
        <NextThemesProvider attribute='class' defaultTheme='light'>
              <StateContext>
                <Layout>
                  <main className={dosis.className}>
                    <Component {...pageProps} />
                  </main>
                </Layout>
              </StateContext>
        </NextThemesProvider>
      </NextUIProvider>
    </>
  );
};

export default MyApp;
