/* eslint-disable react-hooks/rules-of-hooks */
// client_socket_con_nodejs/src/pages/index.tsx
import { Grid } from '@/components/Grid'
import React, { useContext, useState } from 'react'
import { context } from '@/Context/CreateContext'
import { CategoryDevicesElements } from '@/components/CategoryDevicesElements'
import { DevicesElements } from '@/components/DevicesElements'
import { Listbox, ListboxItem, ListboxSection } from '@nextui-org/react'
import { Devices } from '@/interfaces/device.interface'
import { Selection } from '@nextui-org/react'

const home: React.FC = () => {
  const {devices} = useContext(context)
  const [selectedKeys, setSelectedKeys] = useState<any>(new Set(['all']));
  const categoryDevices = Object.keys(devices)
  categoryDevices.push('all')
  let valueSelectedKey: "all" | keyof Devices = 'all'
  selectedKeys.forEach((item: "all" | keyof Devices)=> valueSelectedKey = item)
  const filterDevices: Partial<Devices> = valueSelectedKey === 'all' ? devices : Object.fromEntries([[valueSelectedKey, devices[valueSelectedKey]]]);
  const handleChangeSelectionItemListBox = (keys: Selection) => {
    setSelectedKeys(keys)
  }
  return (
    <>
    <article className='flex flex-row w-5/6 mx-auto my-5'>
        <div className="w-80 h-max border-small px-1 py-2 rounded-small border-default-200 dark:border-default-100 mt-4 top-24 sticky animate__animated animate__backInUp">
          <Listbox
            aria-label="Selecciona una opción"
            variant="faded"
            disallowEmptySelection
            selectionMode="single"
            selectedKeys={selectedKeys}
            onSelectionChange={handleChangeSelectionItemListBox}
            className='capitalize'
            color='primary'
          >
            <ListboxSection title={'Filter category'}>
              {categoryDevices.map((item)=> {
              return (
                <ListboxItem key={item}>{item}</ListboxItem>
              )
            })}
            </ListboxSection>
            
          </Listbox>
        </div>
        
      <Grid elements={filterDevices} Component={CategoryDevicesElements} auxiliaryComponent={DevicesElements}/>
    </article>
    </>
  )
}
export default home