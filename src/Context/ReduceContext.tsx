// client_socket_con_nodejs/src/Context/ReduceContext.tsx
import { IInitialState } from '@/interfaces/context.interfaces/stateContext'
import React from 'react'

export const ReduceContext = (state: any, action: Partial<IInitialState>) => {
  const {welcome, saludo, randomNumber, colorName, devices, user, newStateForDevice, type} = action
  switch(type) {
    case "welcome":
      return {
        ...state,
        welcome: welcome
      }
    case 'saludo':
      return {
        ...state,
        saludo: saludo
      }
    case 'random-number':
      return {
        ...state,
        randomNumber: randomNumber
      }
    case 'color-name':
      return {
        ...state,
        colorName: colorName
      }
    case 'getDevices':
      return {
        ...state,
        devices: devices
      }
    case 'changeUser':
      return {
        ...state,
        user: user
      }
    case 'newStateForDevice':
      return {
        ...state,
        newStateForDevice: newStateForDevice
      }
    default: type
  }
  
}
