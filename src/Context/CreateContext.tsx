// client_socket_con_nodejs/src/Context/CreateContext.tsx
import { ContextValue } from "@/interfaces/context.interfaces/stateContext";
import { createContext } from "react";

export const context = createContext<ContextValue>({} as ContextValue)