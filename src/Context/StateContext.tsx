// client_socket_con_nodejs/src/Context/StateContext.tsx
import React, { useEffect, useReducer, useState} from 'react'
import { io, Socket } from 'socket.io-client';
import { context } from './CreateContext';
import { ReduceContext } from './ReduceContext';
import { Devices } from '@/interfaces/device.interface';
import { TPropOnlyChildren } from '@/interfaces/propOnlyChildren';
import { ContextValue, IInitialState } from '@/interfaces/context.interfaces/stateContext';
import { INewStateForChangeStateDevice } from '@/interfaces/components.interfaces/deviceElement';

export const StateContext: React.FC<TPropOnlyChildren> = ({children}) => {
  const initialState: IInitialState = {
    welcome: [],
    saludo: [],
    randomNumber: [],
    colorName: [],
    devices: {} as Devices,
    user: 'admin',
    newStateForDevice: {} as INewStateForChangeStateDevice
  }
  const [state, dispatch] = useReducer(ReduceContext, initialState)
  const [socket, setSocket] = useState<Socket | undefined>();

  function handleSendMessage  (event: string, message: any) {
    socket ? socket.emit(event, message) : null
  }

  useEffect(() => {
    const newSocket = io('http://localhost:3500');
    setSocket(newSocket);
    newSocket.on("eventosDisponibles", (eventsEmitters: string) => {
      console.log(eventsEmitters)
    })
    newSocket.on("welcome", (welcome: string) => {
      dispatch({
        welcome: welcome,
        type: "welcome"
      })
    })
    
    newSocket.on('saludo', (message: string) => {
      dispatch({
        saludo: message,
        type: 'saludo'
      })
    })

    newSocket.on('random-number', (message: number) => {
      dispatch({
        randomNumber: message,
        type: 'random-number'
      })
    })

    newSocket.on('color-name', (message: string) => {
      dispatch({
        colorName: message,
        type: 'color-name'
      })
    })

    newSocket.on('error-message', (message: string) => {
      console.log(message)
    })

    newSocket.on('dispositivos', (message: Devices) => {
      dispatch({
        devices: message,
        type: 'getDevices'
      })
    })
    newSocket.emit('getDevices', '')

    return () => {newSocket.disconnect()}
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const changeUser = (user: 'user' | 'admin') => {
    dispatch({
      user: user,
      type: 'changeUser'
    })
  }

  const changeContextNewStateDevice = (user: INewStateForChangeStateDevice['user'], category:INewStateForChangeStateDevice['category'], device:INewStateForChangeStateDevice['device'], newState: INewStateForChangeStateDevice['newState']) => {
    const newStateObj = {
      user: user,
      category: category,
      device: device,
      newState: newState
    }
    dispatch({
      newStateForDevice: newStateObj,
      type: 'newStateForDevice'
    })
  }

  const contextValues: ContextValue = {
    welcome: state.welcome,
    saludo: state.saludo,
    randomNumber: state.randomNumber,
    colorName: state.colorName,
    devices: state.devices,
    user: state.user,
    newStateForDevice: state.newStateForDevice,
    changeUser,
    handleSendMessage,
    changeContextNewStateDevice
  }


  return (
    <context.Provider value={contextValues}>
      {children}
    </context.Provider>
  )
}